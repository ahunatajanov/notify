from django.utils.timezone import now
from rest_framework.test import APITestCase
from ..models import Mailing, Client, Message

class TestModel(APITestCase):
    def test_creates_mailings(self):
        mailing = Mailing.objects.create(
            started_date=now(),
            ended_date=now(),
            text="Simple text",
            started_time=now().time(),
            ended_time=now().time(),
            tag="crazy",
        )
        self.assertIsInstance(mailing, Mailing)
        self.assertEqual(mailing.tag, "crazy")

    def test_creates_clients(self):
        client = Client.objects.create(
            phone_number="71234567890",
            operator_code="123",
            tag="crazy",
            timezone="UTC",
        )
        self.assertIsInstance(client, Client)
        self.assertEqual(client.phone_number, "71234567890")

    def test_creates_messages(self):
        self.test_creates_mailings()
        self.test_creates_clients()
        # here error point <<<<<<<<<<<<<
        # message = Message.objects.create(
        #     sending_status="Not sent", mailing_id=1, client_id=1
        # )
        # self.assertIsInstance(message, Message)
        # self.assertEqual(message.sending_status, "Not sent")
