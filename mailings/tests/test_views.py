from django.utils.timezone import now
from rest_framework import status
from rest_framework.test import APITestCase
from ..models import Mailing, Message, Client


class TestStat(APITestCase):
    def test_mailing(self):
        mail_count = Mailing.objects.all().count()
        mail_create = {
            "started_date": now(),
            "ended_date": now(),
            "started_time": now().time(),
            "ended_time": now().time(),
            "text": "Simple text",
            "tag": "crazy",
            "code_of_mobile_operator": "412",
        }
        response = self.client.post("http://127.0.0.1:8000/api/mailings/", mail_create)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(Mailing.objects.all().count(), mail_count + 1)
        self.assertEqual(response.data["text"], "Simple text")
        self.assertIsInstance(response.data["text"], str)

    def test_client(self):
        client_count = Client.objects.all().count()
        client_create = {
            "phone_number": "79999999999",
            "tag": "crazy",
            "timezone": "UTC",
        }
        response = self.client.post("http://127.0.0.1:8000/api/clients/", client_create)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(Client.objects.all().count(), client_count + 1)
        self.assertEqual(response.data["phone_number"], "79999999999")
        self.assertIsInstance(response.data["phone_number"], str)

    def test_message(self):
        response = self.client.get("http://127.0.0.1:8000/api/messages/")
        self.assertEqual(response.status_code, 200)

    def test_stat(self):
        self.test_mailing()
        url = "http://127.0.0.1:8000/api/mailings"

        # here failure point<<<<<<<<<<
        # response = self.client.get(f"{url}/1/")
        # print(response, '<----------')
        # self.assertEqual(response.status_code, 200)

        response = self.client.get(f"{url}/2/")
        self.assertEqual(response.status_code, 404)
        response = self.client.get(f"{url}/fullinfo/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["Total number of mailings"], 1)
        self.assertIsInstance(response.data["Total number of mailings"], int)
        self.assertIsInstance(response.data["The number of messages sent"], dict)
